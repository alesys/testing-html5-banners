/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'pic1',
                            type: 'image',
                            rect: ['0', '0', '300px', '250px', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"pic1.jpg",'0px','0px']
                        },
                        {
                            id: 'pic2',
                            type: 'image',
                            rect: ['0', '0', '300px', '250px', 'auto', 'auto'],
                            overflow: 'visible',
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"pic2.jpg",'0px','0px']
                        },
                        {
                            id: 'cta',
                            type: 'image',
                            rect: ['170px', '251px', '130px', '39px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"cta.png",'0px','0px']
                        },
                        {
                            id: 'bigcopy',
                            type: 'image',
                            rect: ['321px', '103px', '175px', '22px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"bigcopy.png",'0px','0px']
                        },
                        {
                            id: 'smallcopy',
                            type: 'image',
                            rect: ['-178px', '125px', '155px', '17px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"smallcopy.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '300px', '250px'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 6000,
                    autoPlay: true,
                    data: [
                        [
                            "eid4",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${pic2}",
                            '0',
                            '0'
                        ],
                        [
                            "eid5",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${pic2}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid13",
                            "left",
                            4500,
                            500,
                            "easeOutQuad",
                            "${smallcopy}",
                            '-178px',
                            '72px'
                        ],
                        [
                            "eid8",
                            "left",
                            3500,
                            500,
                            "easeOutQuad",
                            "${bigcopy}",
                            '321px',
                            '62px'
                        ],
                        [
                            "eid2",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${pic1}",
                            '0.000000',
                            '1'
                        ],
                        [
                            "eid16",
                            "top",
                            5500,
                            500,
                            "easeOutQuad",
                            "${cta}",
                            '251px',
                            '211px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-88644674");
