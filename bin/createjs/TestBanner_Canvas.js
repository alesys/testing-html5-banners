(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"./bigcopy.png", id:"bigcopy"},
		{src:"./cta.png", id:"cta"},
		{src:"./pic1.jpg", id:"pic1"},
		{src:"./pic2.jpg", id:"pic2"},
		{src:"./smallcopy.png", id:"smallcopy"}
	]
};



// symbols:



(lib.bigcopy = function() {
	this.initialize(img.bigcopy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,175,22);


(lib.cta = function() {
	this.initialize(img.cta);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,130,39);


(lib.pic1 = function() {
	this.initialize(img.pic1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.pic2 = function() {
	this.initialize(img.pic2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.smallcopy = function() {
	this.initialize(img.smallcopy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,155,17);


(lib.Symbol5 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.cta();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,130,39);


(lib.Symbol4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.smallcopy();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,155,17);


(lib.Symbol3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bigcopy();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,175,22);


(lib.Symbol2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.pic2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


(lib.Symbol1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.pic1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,300,250);


// stage content:
(lib.TestBanner_Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_134 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(134).call(this.frame_134).wait(1));

	// bigcopy.png
	this.instance = new lib.Symbol3();
	this.instance.setTransform(400,111.5,1,1,0,0,0,87.5,11);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(83).to({_off:false},0).to({x:150},7,cjs.Ease.get(1)).wait(45));

	// smallcopy.png
	this.instance_1 = new lib.Symbol4();
	this.instance_1.setTransform(-90,131,1,1,0,0,0,77.5,8.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(105).to({_off:false},0).to({x:150},7,cjs.Ease.get(1)).wait(23));

	// cta.png
	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(231,278.5,1,1,0,0,0,65,19.5);
	this.instance_2._off = true;
	new cjs.ButtonHelper(this.instance_2, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(124).to({_off:false},0).to({y:228.5},7,cjs.Ease.get(1)).wait(4));

	// pic2.png
	this.instance_3 = new lib.Symbol2();
	this.instance_3.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(59).to({_off:false},0).to({alpha:1},12).wait(64));

	// pic1.png
	this.instance_4 = new lib.Symbol1();
	this.instance_4.setTransform(150,125,1,1,0,0,0,150,125);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({alpha:1},11).to({_off:true},61).wait(63));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,125,300,250);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;